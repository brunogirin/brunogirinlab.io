---
title: "Banner Images"
date: 2024-08-23T17:58:40+01:00
draft: false
author: Bruno Girin
year: "2024"
month: "2024/08"
image: "4269438067_4b4ba2b87e_o.jpg"
outputs:
- html
- svg
tags:
- code
- hugo
- photography
---

I've wanted to handle images and page resource properly for some time.
Here's how it went and as you can see this post has a nice banner image
in the header so I must have done something right!

<!--more-->

## Image partial

When I set out to do this, I followed a tutorial that used a shortcode to
include images. This works well in the body of a post but not in the header
because the header is in a template: I learnt that you have to use a partial
for that.

OK, that's fine but what if I wanted to include images in the body of a post
and share the code? It appears you can do that by
[calling the partial in a shortcode](https://discourse.gohugo.io/t/can-i-call-a-shortcode-from-a-partial/39043/6).

Here we go, create a partial first and call if from my template, it should all
work fine! Of course not, it appears that when I call the partial, I lose
access to the page context so doing nifty things like
`$.Page.Resources.GetMatch $myImage` doesn't work, which means I can't get a
permalink to the image. I worked around that one by passing the page to my
partial, which means my template has a bit that looks like this:

```go
{{ with .Params.Image }}
  {{- partial "image.html" (dict "page" ($.Page) "image" . ) }}
{{ end }}
```

I can then call `.page.Resources.GetMatch` in the partial, excellent!

## Image metadata

Then I thought: hang on, I also want to pass image metadata, like a caption
and a description. And ideally, I'd like that metadata to be strongly tied
to the image, like having a metadata resource called `myphoto.json` just
next to my image resource called `myphoto.jpg`. Here I learnt about
[replaceRE](https://gohugo.io/functions/strings/replacere/) to transform
the string `myphoto.jpg` into `myphoto.json` and
[transform.Unmarshall](https://gohugo.io/functions/transform/unmarshal/)
to read the content of a JSON file.

## Preserving bandwith

Then I thought (lots of thinking tonight, it's dangerous): I'd like to serve
a small version of the image first to preserve bandwidth and only provide the
full size image in a lightbox. This is where I learnt about
[image processing](https://gohugo.io/content-management/image-processing/)
and the [resize](https://gohugo.io/methods/resource/resize/) function.

## CSS lightbox

As I said, I wanted to have a lightbox but I didn't want to write any
JavaScript for that so I found
[How to Create an Image Lightbox in Pure CSS](https://codesalad.dev/blog/how-to-create-an-image-lightbox-in-pure-css-25).

## Putting it all together

My partial now looks like this:

```go
{{ $imageRes := .page.Resources.GetMatch .image }}
{{ $smallImageRes := $imageRes.Resize "1024x" }}
{{ $dataFile := replaceRE `\.[^.]*$` ".json" .image }}
{{ $lightboxId := replaceRE `\.[^.]*$` "" .image }}
{{ $dataRes := .page.Resources.GetMatch $dataFile }}
{{ $data := dict }}
{{ with $dataRes }}
  {{ with . | transform.Unmarshal }}
    {{ $data = . }}
  {{ end }}
{{ end }}
<figure>
  <a href="#{{ $lightboxId }}">
    <img src="{{ $smallImageRes.RelPermalink }}" {{ with $data.description }}alt="{{ . }}"{{ end }} >
  </a>
  {{ with $data.caption }}
    <figcaption>{{ . }}</figcaption>
  {{ end }}
</figure>

<a href="#" class="lightbox" id="{{ $lightboxId }}">
  <span style="background-image: url('{{ $imageRes.RelPermalink }}')"></span>
</a>
```

It fiddles with the name of the image to find the name of the associated
JSON file and generate an ID for the lightbox, loads the metadata and
then spews out a nice bit of HTML.

## Styling it all

The styling for the `figure` part is fairly simple, center everything and
make the image fit the width:

```CSS
figure {
  margin: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
}

header > figure {
  margin-top: 1em;
}

figure img {
  width: 100%;
}
```

The trick comes with the lightbox where I learnt about the
[`:target` pseudo-class](https://developer.mozilla.org/en-US/docs/Web/CSS/:target).
We just hide the lightbox by default and only show it when it is the target,
which happens when you click on the link the references it, which in this case
is the small image. As soon as you click out of it, it's no longer the target
so it hides again. The CSS is nearly a copy of what Gregory Schier did in his
lightbox article:

```CSS
.lightbox {
  display: none;
  position: fixed;
  z-index: 999;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  padding: 1em;
  background: rgba(0, 0, 0, 0.7);
}

.lightbox:target {
  display: block;
}

.lightbox span {
  display: block;
  width: 100%;
  height: 100%;
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
}
```

## Open Graph

The final touch is to change the Open Graph `og:image` `meta` tag when you
have a banner image. And to be nice to social media clients, use the small
version:

```go
{{ with .Params.Image }}
  {{ $imageRes := $.Page.Resources.GetMatch . }}
  {{ $smallImageRes := $imageRes.Resize "1024x" }}
  <meta property="og:image" content="{{ $smallImageRes.Permalink }}" />
{{ else with .OutputFormats.Get "svg" }}
  <meta property="og:image" content="{{ .Permalink }}" />
{{ end }}
```

You might have noticed that I really like the Hugo template
[with](https://gohugo.io/functions/go-template/with/) operator: it is very
versatile and helps make my templates quite concise.

## Shortcode

I created a shortcode too but I haven't tried it yet, this will be a task
for another day.
