---
title: "Dark Mode"
date: 2024-08-21T20:03:58+01:00
draft: false
author: Bruno Girin
year: "2024"
month: "2024/08"
outputs:
- html
- svg
tags:
- code
- CSS
---

Between people I follow on Mastodon and some of my work colleagues,
I've been following discussions on dark mode and thought it would be
a good addition to this site. So here is what I did.

<!--more-->

I don't really have a strong preference for light or dark mode when
using my computer. However, one of my colleagues who is visually impaired
explained to us recently how dark mode is a lot easier for him to use and
how offering that ability on your site is a good accessibility improvement.

Considering the styling of this site is still minimal, I assumed it wouldn't
be too much effort to build this in a way that follows browser preferences.

## Step 1: CSS custom properties

The first step was to narrow down the scope of the problem by moving most of
my colour definitions to CSS custom properties. This way, all future changes
to my CSS code would use the defined properties and get dark mode support out
of the box. This is also a good way to classify the colours I use and start
building the skeleton of a design system in my CSS code. I now have a `:root`
rule that defines all light mode colours and looks like this:

```CSS
:root {
  --main-bg-color: white;
  --main-color: black;
  --accent-bg-color: #393;
  --accent-color: #cfc;
  --secondary-bg-color: #eee;
  --secondary-color: #333;
  --secondary-border-color: #ccc;
  --quote-color: #393;
  --link-color: #00e;
  --link-visited-color: #551a8b;
  --link-active-color: #f00;
}
```

Then I moved all the colour references that needed to change to CSS properties
such as:

```CSS
html, body {
  background-color: var(--main-bg-color);
  color: var(--main-color);
}
```

## Step 2: Dark mode media query

With that done, it becomes very easy to override some of those properties inside
a dark mode media query:

```CSS
@media (prefers-color-scheme: dark) {
  :root {
    --main-bg-color: #111;
    --main-color: #eee;
    --secondary-bg-color: #333;
    --secondary-color: #eee;
    --secondary-border-color: #666;
    --link-color: #8bf;
    --link-visited-color: #a7e;
    --link-active-color: #f66;
  }
}
```

And there you have it! Easy dark mode that follows the browser's preferences.

## Don't swap out everything

Creating a dark mode doesn't have to swap all colour combinations. You will
notice that in light mode, the header on this site is black and so are the
code blocks. It stays the same in dark mode as I feel it works better that way.

Mermaid diagrams don't change either because the mermaid output embeds styling
rules and I haven't found how to override them reliably.

Finally, the accent colours stay the same as the dark green I've used works
well on top of a light or dark background.

## Choosing dark mode colours

You will notice that my dark mode is not white on black, it's rather off-white
on off-black. This is done on purpose as it's easier on the eye. The hardest
colours to choose were the link colours. There is no science to this: I just
fiddled with the numbers until I liked the way it looked.
