---
title: "More Image Experiments"
date: 2024-08-24T19:09:23+01:00
draft: false
author: Bruno Girin
year: "2024"
month: "2024/08"
image: "9693754197_a016441ab1_o.jpg"
outputs:
- html
- svg
tags:
- code
- hugo
- photography
---

After last night's success, time to include a photograph in the body of the
article.

<!--more-->

Now that I have banner images working, I want to be able to include images in
article bodies like so:

{{< image "9696981422_c24cba80fc_o.jpg" >}}

It works a charm, simply by creating a shortcode that calls the partial that
I created for the banner image:

```go
{{ partial "image.html" (dict "page" $.Page "image" (.Get 0)) }}
```

I then need to call this shortcode in the body of the article with a single
parameter, the path to the image, relative to the article file. After that,
a bit of styling makes body images consistent with banner images:

```CSS
article > figure {
  background-color: var(--secondary-bg-color);
  color: var(--secondary-color);
  padding: 0.5rem;
  font-size: 90%;
  border-bottom: 1px solid var(--secondary-border-color);
  border-top: 1px solid var(--secondary-border-color);
}
```

Being able to reuse code in a different context and have it work first time
is very satisfying. I'd better stop fiddling with my websie while I'm winning!
For those wondering, the photographs in this post were taken by a
[P-Sharan WIDE-35 paper pinhole camera](https://www.retrotogo.com/2009/12/sharan-wide-35-panoramic-pinhole-camera.html),
probably on bog standard 400 ISO colour film.
