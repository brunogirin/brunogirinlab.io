---
title: "Hello, World!"
date: 2024-06-01T17:18:55+01:00
draft: false
author: Bruno Girin
year: "2024"
month: "2024/06"
tags:
- hello
outputs:
- html
- svg
---

Hello, World! Or rather: Hello, Reader!

<!--more-->

When I first created this site, aeons ago, I asked the question:

> I wanted to say that you'll find a lot of interesting things on this
> site but to be fair, I have no clue what I'll be writing here yet.
> So watch this space, I guess?

I've now decided what to do with this space so here goes. I'll use this site
to post links to interesting sites or any other musing that comes to mind
that I want to keep somewhere. I realise that I often find resources online,
stash it somewhere or post it on social media thinking that I'll remember
what it was. Of course, when I am actively looking for it, I don't find it
anymore. So this web site is an attempt for me to keep track of things that
interest me.

I realise that this means the content of this site may not always be of
interest to you, dear Reader, but I hope it will sometimes be.

It also means that the frequency of updates will be rather erratic, as I
will only add content when I find something interesting to me, which may
not happen for months at a time.