---
title: "Bringing Colour to my Posts"
date: 2024-06-16T14:52:02+01:00
draft: false
author: Bruno Girin
year: "2024"
month: "2024/06"
outputs:
- html
- svg
tags:
- code
- hugo
- SVG
---

Up until now, this website has been very monochrome with no pictures.
I decided to do something about it by generating SVG code in a template.

<!--more-->

## The idea

The posts on this site so far are all plain text and I feel that this will
often be the case. Even though, I thought it would still be nice to offer
an image with each post, in a bid to add a bit of colour to the site. On the
other hand, I didn't want to start down a path where I would absolutely have
to upload an image with each post, as I know this will quickly become the
annoying hurdle that will make me lose interest in this blog.

Could I generate one instead? Using nothing more than Hugo templates?
Hugo templates manipulate text and [SVG](https://en.wikipedia.org/wiki/SVG)
is a text based image format so that looked promising.

I was initially dreaming of generating fantastically fluid shapes that would
look amazing then realised that neither Hugo templates nor my drawing skills
were up to scratch for this so I settled on a simple set of requirements:

- All calculations should be doable using the Hugo template language,
- Shapes should be simple to draw,
- The result should look random while being stable,
- It should work on a black background.

## The implementation

### Enabling SVG in Hugo

My biggest uncertainty was whether Hugo was up to the task so I started by
searching the Hugo docs to find out how I could configure SVG output. The
[Custom output formats](https://gohugo.io/templates/output-formats/) page
sounded like what I wanted. Through trial and error, and repeated readings
of this page, I finally understood that even though SVG is a built-in
**media type**, it is not a built-in **output format**, which meant I had
a few more things to declare than I originally thought. Then my Hugo
implementation complained that it wasn't a defined media type either so I
had to define that too. And specify on what output I wanted to use it.

The final configuration looks like this:

```toml
[mediaTypes]
  [mediaTypes.'image/svg+xml']
    suffixes = 'svg'

[outputFormats]
  [outputFormats.svg]
    baseName = 'index'
    isPlainText = false
    mediaType = 'image/svg+xml'
    notAlternative = true

[outputs]
  home = ['html', 'rss', 'svg']
  page = ['html', 'svg']
  rss = ['rss']
  section = ['html', 'rss']
  taxonomy = ['html', 'rss']
  term = ['html', 'rss']
```

Then each post I want to generate an image for needs to have that output
added in its front matter:

```yaml
outputs:
- html
- svg
```

Once that is done, each post has an `index.svg` page next to its
`index.html` one. Time to start coding a template!

Before starting, a short note on a Hugo quirk: according to
[the documentation on templates for custom formats](https://gohugo.io/templates/output-formats/#templates-for-your-output-formats),
they need to be named with the format
`[partial name].[OutputFormat].[suffix]`, which means that my SVG template
for a single post is named `single.svg.svg`. Weird but it works.

### Source of randomness

One of my requirements is for some aspects of the drawing to look random
while being stable, that is the drawing should always be the same for the
same post. Each post has 3 parts:
- A title,
- A summary,
- A content.

And for each, Hugo can tell me the number of words and the number of
characters. This gives me 6 values that stay stable for each post but are
fairly random from one post to the next. So I use this as an initial
source of randomness:

```go
{{ $tw := strings.CountWords .Title }}
{{ $tr := strings.CountRunes .Title }}
{{ $sw := strings.CountWords .Summary }}
{{ $sr := strings.CountRunes .Summary }}
{{ $cw := strings.CountWords .Content }}
{{ $cr := strings.CountRunes .Content }}
```

From those, I use the `mod` operator to select:
- A colour scheme: it's Pride month so I decided to go with 6 of
  [the Pride flags](https://www.kapwing.com/resources/official-pride-colors-2021-exact-color-codes-for-15-pride-flags/#classic-rainbow);
- An angle to rotate the shapes by;
- A shape out of a list of regular polygons: triangle, square, pentagon,
  5-pointed star, hexagon, octagon.

Then there's the question of placing shapes in the image in a way that looks
random. Past experience suggests that a good way to do this is to use a
monotically increasing sequence of numbers and apply a modulo to each of them.
It works even better if the module is a prime number. This is why my SVG
images have an unusual view box of 1021x509 because those are the two prime
numbers closest to 1024 and 512, which were my initial values.

So I create a sequence:

```go
{{ $seeds := seq 200 }}
```

multiply each number by one of my random seeds and apply a modulo. However,
using the same trick for the `x` and `y` attributes of each shape breaks the
randomness by creating parallel lines of shapes. To fix that, I added a `log`
factor to the `y` calculation.

### Putting it all together

With that done, it's a case of creating a SVG template with a large preamble
that sets all the variable that are static for the whole image and then
iterate over the items in `seeds` to create shapes.

Each shape is centered around the origin to make it easier to transform it,
in order: rotate, scale, translate.

## Displaying the result

To display the result, I need to reference the resulting SVG file in the
HTML template. However, I want that image to act as a background to my
header so I can't include it with an `img` tag. Instead, I added a small
piece of inline style to the `header` tag:

```go
<header {{ with .OutputFormats.Get "svg" }} style="background-image: url('{{ .RelPermalink }}')"{{ end }}>
```

The `with .OutputFormats.Get "svg"` construct ensures that it will only be
added if a corresponding SVg output exists.

Then to make sure the image is not repeated but fills as much of the header
as possible, I added a `background-size: cover` CSS rule to the header.

The last piece was to add a `background-color: rgba(0, 0, 0, 0.7)` CSS
rule to the `h1` and `nav` tags so that they are still legible when
above shapes.

## Side quests

### Open Graph

Now that I have an image for each post, it reminded me that I could add
[Open Graph](https://ogp.me/) tags in each page's `head`. Off we go,
adding `og:title`, `og:type`, `og:url`, `og:image`, `og:locale` and
`og:site_name` was easy to do.

But then, `og:locale` led me to...

### Language code

Hugo has a `languageCode` parameter in the site configuration and,
according to
[WCAG success criterion 3.1.1](https://www.w3.org/WAI/WCAG21/Understanding/language-of-page.html),
you should always use it to help accessibility tools identify the
language programmatically. But then, as a French person I thought
there was a chance some of my posts would be in French. The simplest
thing to do was to allow for an override at page level and deal
with it in the Open Graph `meta` tag and each the `html` tag:

```go
<html lang="{{ with .Params.LanguageCode }}{{ . }}{{ else }}{{ .Site.LanguageCode }}{{ end }}">
```

And then, that also meant including it with each entry in list pages
in case I have a list that mixes posts in different languages.

### Code highlights

While writing this post, I realised that if you include a fenced code block
in your text and specify the language, Hugo will automatically apply static
syntax highlighting. Neat! However, it doesn't know `hugo` as a language so
you have to use `go` instead.

## Conclusion

You now know how posts on this website acquired automatically generated
SVG images. It was fun to do and I'm rather happy with the result. In an
ideal world, I will now expand this to all other pages. That's work for
another day though.

Here is the final SVG template:

```go
{{/* Base data taken from the post */}}
{{ $tw := strings.CountWords .Title }}
{{ $tr := strings.CountRunes .Title }}
{{ $sw := strings.CountWords .Summary }}
{{ $sr := strings.CountRunes .Summary }}
{{ $cw := strings.CountWords .Content }}
{{ $cr := strings.CountRunes .Content }}
{{/* Colour schemes */}}
{{ $rainbow := slice "#e50000" "#ff8d00" "#ffee00" "#028121" "#004cff" "#770088" }}
{{ $transgender := slice "#5bcffb" "#f5abb9" "#ffffff" "#f5abb9" "#5bcffb" }}
{{ $bisexual := slice "#d60270" "#d60270" "#9b4f96" "#0038a8" "#0038a8" }}
{{ $pansexual := slice "#ff1c8d" "#ffd700" "#1ab3ff" }}
{{ $lesbian := slice "#d62800" "#ff9b56" "#ffffff" "#d462a6" "#a40062" }}
{{ $genderqueer := slice "#b57fdd" "#ffffff" "#49821e" }}
{{ $schemes := slice $rainbow $transgender $bisexual $pansexual $lesbian $genderqueer }}
{{ $scheme := index $schemes (mod $sw (len $schemes)) }}
{{/* Size of display: closest primes to 1024 and 512 */}}
{{ $vw := 1021 }}
{{ $vh := 509 }}
{{/* Angle of the shapes */}}
{{ $angle := mul 12 (mod $sr 30) }}
{{/* Shape and corresponding path */}}
{{ $shape := mod $tr 6 }}
{{ $path := "" }}
{{ if (eq $shape 0) }}
  {{ $path = "M1 0L-0.5 0.866L-0.5 -0.866Z" }}
{{ else if (eq $shape 1) }}
  {{ $path = "M1 0L0 1L-1 0L0 -1Z" }}
{{ else if (eq $shape 2) }}
  {{ $path = "M1 0L0.309 0.951L-0.809 0.588L-0.809 -0.588L0.309 -0.951Z" }}
{{ else if (eq $shape 3) }}
  {{ $path = "M1 0L-0.809 0.588L0.309 -0.951L0.309 0.951L-0.809 -0.588Z" }}
{{ else if (eq $shape 4) }}
  {{ $path = "M1 0L0.5 0.866L-0.5 0.866L-1 0L-0.5 -0.866L0.5 -0.866Z" }}
{{ else }}
  {{ $path = "M1 0L0.707 0.707L0 1L-0.707 0.707L-1 0L-0.707 -0.707L0 -1L0.707 -0.707Z" }}
{{ end }}
{{/* Prep for loop: initialise x, y, scale, fill and get some seeds */}}
{{ $x := 0 }}
{{ $y := 0 }}
{{ $scale := 0 }}
{{ $fill := "black" }}
{{ $seeds := seq 200 }}
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 {{ $vw }} {{ $vh }}">
  <text x="20" y="20">{{ $tw }}-{{ $tr }}-{{ $sw }}-{{ $sr }}-{{ $cw }}-{{ $cr }}</text>
  {{ range $seed := $seeds }}
    {{ $x = mod (mul $seed 5 $cr) $vw }}
    {{ $y = mod (mul $seed (math.Log $seed) $cw) $vh }}
    {{ $scale = add (mod (mul $seed 3) 11) 5 }}
    {{ $fill = index $scheme (div (mul $x (len $scheme)) $vw) }}
    <g transform="translate({{ $x }} {{ $y }})">
      <g transform="scale({{ $scale }})">
        <g transform="rotate({{ $angle }})">
          <path d="{{ $path }}" fill="{{ $fill }}"></path>
        </g>
      </g>
    </g>
  {{ end }}
</svg>
```
