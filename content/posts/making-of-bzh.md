---
title: "Making of Bzh"
date: 2024-06-09T15:03:51+01:00
draft: false
author: Bruno Girin
year: "2024"
month: "2024/06"
tags:
- code
- hugo
- CI
outputs:
- html
- svg
---

This website is made with [Hugo](https://gohugo.io/) using a custom layout,
archetypes and design. Don't let that word _custom_ fool you though: it's
all fairly simple.

<!--more-->

The code for this website is available
[on Gitlab](https://gitlab.com/brunogirin/brunogirin.gitlab.io) and licensed
under [GPL v3](https://gitlab.com/brunogirin/brunogirin.gitlab.io/-/blob/main/LICENSE)
so feel free to have a look and copy as much of it as you want.

It uses Gitlab Pages for hosting with a custom domain name: it's free and works
extremely well with a tool like Hugo.

The site is rebuilt every time I push to Gitlab using Gitlab-CI. The
[CI script](https://gitlab.com/brunogirin/brunogirin.gitlab.io/-/blob/main/.gitlab-ci.yml)
is very short and good a example of how to create a minimal Gitlab Pages script.
The only gotcha is the
[update-hugo.sh script](https://gitlab.com/brunogirin/brunogirin.gitlab.io/-/blob/main/bin/update_hugo.sh)
that I created from the Computing for Geeks
[_Install and Use Hugo on Ubuntu_ article](https://computingforgeeks.com/how-to-install-hugo-on-ubuntu-debian/).
I did that because Ubuntu, which I use for the CI script, doesn't have a recent
version of Hugo in its repository and isolating those few lines of code in a
script means I can also use it when installing Hugo in my local machine:
I use a Windows laptop with Windows Subsystem for Linux and an Ubuntu VM to
update the site.

In terms of Hugo code, I've basically been following Brian P. Hogan's
[_Build Websites with Hugo_ book](https://bphogan.com/2020/02/12/build-websites-with-hugo/)
with a few tweaks to make it look and behave the way I wanted. When stuck
I've also been relying on the comprehensive
[Hugo documentation](https://gohugo.io/documentation/).

The CSS code is mostly my own so if this website looks ugly it is entirely
my fault.