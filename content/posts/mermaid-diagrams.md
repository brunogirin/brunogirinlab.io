---
title: "Mermaid Diagrams"
date: 2024-06-22T14:00:47+01:00
draft: false
author: Bruno Girin
year: "2024"
month: "2024/06"
outputs:
- html
- svg
---

I was wondering how to enable [Mermaid diagrams](https://mermaid.js.org/)
in Hugo.

<!--more-->

I followed the
[Mermaid diagrams Hugo documentation](https://gohugo.io/content-management/diagrams/#mermaid-diagrams),
Where the Hugo documentation says _include this snippet at the bottom of
the content template_, I added it at the bottom of the `baseof.html`
template, just before the closing `</body>` tag. It worked out of the box.

In theory, you should now see a diagram below, that shows a box labelled
_A_, out of which comes an arrow that goes to a box labelled _B_:

```mermaid
flowchart LR
    A --> B
```

You can also add front matter to a chart so the one below should show the
same diagram with a title that says _Now with a title_:

```mermaid
---
title: Now with a title
---
flowchart LR
    A --> B
```

One thing to note with Mermaid diagrams: because of the way it works, each
diagram includes a `<style>` block that enforces a set of style rules
for that given diagram. It's designed to be overriden through the
[Mermaid theme configuration](https://mermaid.js.org/config/theming.html)
but it's unclear how to do that for a whole site. Some research needed to
work it out properly. In the meantime, I'll just use the default theme.

On a related subject, reading through the documentation above made me realise
that Hugo has [render hooks](https://gohugo.io/render-hooks/introduction/)
that make it possible to tweak the overall rendering of certain components.
More research needed here too!
