---
outputs:
- html
- rss
- svg
---
This is my home on the internet. My name is Bruno and I am a French
person who lives in London, UK, and works in technology in the
ESG (Environmental, Social and Governance) space, with a focus on
the "E" part.

This site is a place for me to keep track of things I have an
interest in and post some musing from time to time. It may go several
months without an update.

## Posts

Most of the content is in the [posts](./posts) section so head there
for the details. At some point, I'll work out how to bring the latest
few posts into the index page so that you don't have to click through.
This site is built with [Hugo](https://gohugo.io/) so if you've got
experience with it, feel free to suggest improvements.

## Look & Feel

You might have noticed that I am not a designer, which is why I've
stuck with a minimalist design and a monochrome palette.

{{< social >}}
