# bzh.me.uk / brunogirin.gitlab.io

This is my personal website. Its main home is [bzh.me.uk](https://bzh.me.uk).
It has a secondary home at [brunogirin.gitlab.io](https://brunogirin.gitlab.io).

The site is built using the static site generator [Hugo](https://gohugo.io/).
It is rebuilt every time a modification is pushed to this repository using
a Gitlab CI pipeline.

The code is licenced under [GPL v3](./LICENSE) and the content is
[Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
