#!/usr/bin/env bash
curl -s https://api.github.com/repos/gohugoio/hugo/releases/latest | jq -r '[.assets[].browser_download_url | select(test("hugo_[0-9].*linux-amd64.deb$"))][0]' | wget -q -i -
dpkg -i hugo*_linux-amd64.deb
