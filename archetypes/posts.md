---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: false
author: Bruno Girin
year: "{{ .Date | dateFormat "2006" }}"
month: "{{ .Date | dateFormat "2006/01" }}"
outputs:
- html
- svg
---

The summary of the post goes here.

<!--more-->

The body of the post goes here.
